## SNAP-2DFE Synchronized NAtural and Posed 2D Facial Expressions

In order to quantify the impact of free head movements on expression recognition performances, we propose an innovative acquisition system that collects data simultaneously in presence and absence of head movement.

![SNAP2DFE GIF](files/snap2dfe_small.gif)

Each facial expression is recorded simultaneously using a two-camera system : one camera is fixed on a helmet, while the other is placed in front of the user at near-range distance. Our database enhances measuring the impact of head-movements relying on the information returned by the frontal camera, compared to the helmet one. In each sequence, the user follows a specific pattern of movement that corresponds to one of the following animations : one translation on Ox, combined with three rotations (roll, yaw, pitch).

![SNAP2DFE PNG](files/SNAP2DFE_small.png)

Facial landmarks are provided as annotations on both cameras. Sequences are also annotated with type of acted expressions (Neutral, Happiness, Fear, Anger, Disgust, Sadness, Surprise). A 3-axis gyroscope provides head orientation information throughout the sequence.

Dataset size: 93.240 annotated frames - 1260 videos collected from 15 subjects.

Contact : IM. BILASCO marius.bilasco@univ-lille.fr

![User agreement](files/SNAP2DFE_user_agreement.pdf)

## Demo
https://pod.univ-lille.fr/video/4794-snap-2dfe-synchronized-natural-and-posed-2d-facial-expressions/
